# Custom CKEditor 5 Build

Build:
```bash
npm install & npm build
```

Includes this packages as dev dependency:

- "@ckeditor/ckeditor5-alignment"
- "@ckeditor/ckeditor5-autoformat"
- "@ckeditor/ckeditor5-basic-styles"
- "@ckeditor/ckeditor5-block-quote"
- "@ckeditor/ckeditor5-dev-utils"
- "@ckeditor/ckeditor5-dev-webpack-plugin"
- "@ckeditor/ckeditor5-editor-classic"
- "@ckeditor/ckeditor5-essentials"
- "@ckeditor/ckeditor5-heading"
- "@ckeditor/ckeditor5-image"
- "@ckeditor/ckeditor5-indent"
- "@ckeditor/ckeditor5-link"
- "@ckeditor/ckeditor5-list"
- "@ckeditor/ckeditor5-media-embed"
- "@ckeditor/ckeditor5-paragraph"
- "@ckeditor/ckeditor5-paste-from-office"
- "@ckeditor/ckeditor5-table"
- "@ckeditor/ckeditor5-theme-lark"
- "@ckeditor/ckeditor5-typing"

